package application

import (
	"gitlab.com/zhongjie.cai/WebServiceTemplate/certificate"
	"gitlab.com/zhongjie.cai/WebServiceTemplate/config"
	"gitlab.com/zhongjie.cai/WebServiceTemplate/logger"
	"gitlab.com/zhongjie.cai/WebServiceTemplate/server"
)

// func pointers for injection / testing: main.go
var (
	configInitialize          = config.Initialize
	certificateInitialize     = certificate.Initialize
	loggerInitialize          = logger.Initialize
	loggerAppRoot             = logger.AppRoot
	serverHost                = server.Host
	doPreBootstrapingFunc     = doPreBootstraping
	bootstrapApplicationFunc  = bootstrapApplication
	doPostBootstrapingFunc    = doPostBootstraping
	doApplicationStartingFunc = doApplicationStarting
	doApplicationClosingFunc  = doApplicationClosing
)
