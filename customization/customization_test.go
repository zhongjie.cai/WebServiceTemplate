package customization

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/zhongjie.cai/WebServiceTemplate/logger/logtype"
	"gitlab.com/zhongjie.cai/WebServiceTemplate/server/model"
	"gitlab.com/zhongjie.cai/WebServiceTemplate/session"
)

func TestReset(t *testing.T) {
	// stub
	PreBootstrapFunc = func() error { return nil }
	PostBootstrapFunc = func() error { return nil }
	AppClosingFunc = func() error { return nil }
	LoggingFunc = func(session *session.Session, logType logtype.LogType, category, subcategory, description string) {}
	AppVersion = func() string { return "" }
	AppPort = func() string { return "" }
	AppName = func() string { return "" }
	AppPath = func() string { return "" }
	IsLocalhost = func() bool { return false }
	ServeHTTPS = func() bool { return false }
	ServerCertContent = func() string { return "" }
	ServerKeyContent = func() string { return "" }
	ValidateClientCert = func() bool { return false }
	CaCertContent = func() string { return "" }
	Routes = func() []model.Route { return nil }
	Statics = func() []model.Static { return nil }

	// mock
	createMock(t)

	// SUT + act
	Reset()

	// assert
	assert.Nil(t, PreBootstrapFunc)
	assert.Nil(t, PostBootstrapFunc)
	assert.Nil(t, AppClosingFunc)
	assert.Nil(t, LoggingFunc)
	assert.Nil(t, AppVersion)
	assert.Nil(t, AppPort)
	assert.Nil(t, AppName)
	assert.Nil(t, AppPath)
	assert.Nil(t, IsLocalhost)
	assert.Nil(t, ServeHTTPS)
	assert.Nil(t, ServerCertContent)
	assert.Nil(t, ServerKeyContent)
	assert.Nil(t, ValidateClientCert)
	assert.Nil(t, CaCertContent)
	assert.Nil(t, Routes)
	assert.Nil(t, Statics)

	// verify
	verifyAll(t)
}
