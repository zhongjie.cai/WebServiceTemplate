package logger

import (
	"fmt"

	"github.com/google/uuid"
	"gitlab.com/zhongjie.cai/WebServiceTemplate/apperror"
	"gitlab.com/zhongjie.cai/WebServiceTemplate/jsonutil"
	"gitlab.com/zhongjie.cai/WebServiceTemplate/session"
	"gitlab.com/zhongjie.cai/WebServiceTemplate/timeutil"
)

// func pointers for injection / testing: logger.go
var (
	fmtPrintf                  = fmt.Printf
	fmtPrintln                 = fmt.Println
	uuidNew                    = uuid.New
	uuidParse                  = uuid.Parse
	fmtSprintf                 = fmt.Sprintf
	timeutilGetTimeNowUTC      = timeutil.GetTimeNowUTC
	jsonutilMarshalIgnoreError = jsonutil.MarshalIgnoreError
	sessionGet                 = session.Get
	apperrorWrapSimpleError    = apperror.WrapSimpleError
	defaultLoggingFunc         = defaultLogging
	prepareLoggingFunc         = prepareLogging
)
