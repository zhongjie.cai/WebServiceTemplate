package handler

import (
	"github.com/gorilla/mux"
	"gitlab.com/zhongjie.cai/WebServiceTemplate/apperror"
	"gitlab.com/zhongjie.cai/WebServiceTemplate/logger"
	"gitlab.com/zhongjie.cai/WebServiceTemplate/request"
	"gitlab.com/zhongjie.cai/WebServiceTemplate/response"
	"gitlab.com/zhongjie.cai/WebServiceTemplate/server/panic"
	"gitlab.com/zhongjie.cai/WebServiceTemplate/server/route"
	"gitlab.com/zhongjie.cai/WebServiceTemplate/session"
)

// func pointers for injection / testing: common.go
var (
	muxVars                     = mux.Vars
	routeGetRouteInfo           = route.GetRouteInfo
	sessionRegister             = session.Register
	sessionUnregister           = session.Unregister
	panicHandle                 = panic.Handle
	requestGetLoginID           = request.GetLoginID
	requestGetCorrelationID     = request.GetCorrelationID
	requestGetAllowedLogType    = request.GetAllowedLogType
	requestGetRequestBody       = request.GetRequestBody
	responseWrite               = response.Write
	loggerAPIEnter              = logger.APIEnter
	loggerAPIExit               = logger.APIExit
	apperrorGetInvalidOperation = apperror.GetInvalidOperation
)
