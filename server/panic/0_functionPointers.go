package panic

import (
	"fmt"

	"gitlab.com/zhongjie.cai/WebServiceTemplate/apperror"
	"gitlab.com/zhongjie.cai/WebServiceTemplate/logger"
	"gitlab.com/zhongjie.cai/WebServiceTemplate/response"
)

// func pointers for injection / testing: panic.go
var (
	fmtErrorf                      = fmt.Errorf
	getRecoverErrorFunc            = getRecoverError
	loggerAppRoot                  = logger.AppRoot
	responseWrite                  = response.Write
	apperrorGetGeneralFailureError = apperror.GetGeneralFailureError
	getDebugStackFunc              = getDebugStack
)
