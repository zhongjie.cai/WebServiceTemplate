package config

import (
	"fmt"
	"reflect"

	"gitlab.com/zhongjie.cai/WebServiceTemplate/apperror"
	"gitlab.com/zhongjie.cai/WebServiceTemplate/timeutil"
)

// func pointers for injection / testing: config.go
var (
	timeutilGetTimeNowUTC            = timeutil.GetTimeNowUTC
	timeutilFormatDateTime           = timeutil.FormatDateTime
	apperrorWrapSimpleError          = apperror.WrapSimpleError
	apperrorConsolidateAllErrors     = apperror.ConsolidateAllErrors
	reflectValueOf                   = reflect.ValueOf
	fmtSprintf                       = fmt.Sprintf
	functionPointerEqualsFunc        = functionPointerEquals
	isServerCertificateAvailableFunc = isServerCertificateAvailable
	isCaCertificateAvailableFunc     = isCaCertificateAvailable
	validateStringFunctionFunc       = validateStringFunction
	validateBooleanFunctionFunc      = validateBooleanFunction
)
